<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;

use Location\Coordinate;
use Location\Distance\Vincenty;

class DistanceCalculator {

	/**
	 * @param array  $from
	 * @param array  $to
	 * @param string $unit - m, km
	 *
	 * @return mixed
	 */
	public function calculate( $from, $to, $unit = 'm' )
    {
        $coordinate1 = new Coordinate( $from[0], $from[1] );
        $coordinate2 = new Coordinate( $to[0], $to[1] );
        $calculator = new Vincenty();

        $distance = $calculator->getDistance( $coordinate1, $coordinate2 );

        if($unit = 'km') {
            return $distance / 1000;
        }

        return $distance;
	}

	/**
	 * @param array $from
	 * @param array $offices
	 *
	 * @return array
	 */
	public function findClosestOffice( $from, $offices )
    {
	    $meters = null;
        $closest = null;

        foreach ( $offices as $office )
        {
            $distance = $this->calculate( $from, [$office['lat'], $office['lng']], 'm' );

            if ($meters === null || ($meters !== null && $meters >= $distance)) {
                $meters = $distance;
                $office['distance'] = $distance;
                $closest = $office;
            }
        }

        return $closest;
	}

}
