<?php
/**
 * Date: 09/08/2018
 * Time: 00:16
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;

class ResponseTimeCalculator implements ResponseTimeCalculatorInterface  {

    public function calculate( $bookingDateTime, $responseDateTime, $officeHours )
    {
        $begin = Carbon::parse($bookingDateTime);
        $end = Carbon::parse($responseDateTime);

        $responseTime = $begin->diffFiltered(CarbonInterval::minute(), function ($date) use($officeHours) {
            $dayOfWeek = Carbon::parse($date)->dayOfWeek;
            if(
                !$officeHours[$dayOfWeek]['isClosed']
                && $date >= Carbon::parse($date->format('Y-m-d') . ' ' . $officeHours[$dayOfWeek]['from'])
                && $date <= Carbon::parse($date->format('Y-m-d') . ' ' . $officeHours[$dayOfWeek]['to'])
            ) return true;

        }, $end);

        return $responseTime;
    }
}
